import React from 'react';
import { Form, FloatingLabel, Row, Col, Button } from 'react-bootstrap';
import flower from '../images/flower.png';

const LogIn = () => {
  return (
    <div className="userLogIn">
      <img src={flower} alt="flower.png" />
      <div className="form">
        <h3>Sign In</h3>

        <Form className="d-flex flex-column justify-content-center align-items-center m-5 p-5">
          <Form.Group className="pb-3 mb-3" controlId="formGridEmail">
            <Form.Label>Email Address</Form.Label>
            <Form.Control type="email" placeholder="Enter email address" />
          </Form.Group>

          <Form.Group className="pb-3 mb-3" controlId="formGridPassword">
            <Form.Label>Password</Form.Label>
            <Form.Control type="number" placeholder="Enter password" />
          </Form.Group>

          <div className="d-flex">
            <Button className="m-auto p-3" variant="primary" type="submit">
              Log in
            </Button>
          </div>
        </Form>
      </div>
    </div>
  );
};

export default LogIn;
