import React from 'react';
import { useState, useEffect } from 'react';
import { Form, Button, Row, Col } from 'react-bootstrap';
import flower from '../images/flower.png';
import { Redirect } from 'react-router';
import Swal from 'sweetalert2';
import { useHistory } from 'react-router-dom';

const Register = () => {
  const history = useHistory();

  const [user, setUser] = useState({
    id: null,
    isAdmin: null,
  });

  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [email, setEmail] = useState('');
  const [mobileNo, setMobileNo] = useState('');
  const [password1, setPassword1] = useState('');
  const [password2, setPassword2] = useState('');

  const [isActive, setIsActive] = useState(false);

  function registerUser(e) {
    e.preventDefault();

    fetch('http://localhost:4000/users/register', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        firstName: firstName,
        lastName: lastName,
        email: email,
        mobileNo: mobileNo,
        password: password1,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        console.log('data', data);
        if (typeof data.message === 'undefined') {
          localStorage.setItem('token', data.access);
          retrieveUserDetails(data.access);

          Swal.fire({
            icon: 'success',
            title: 'Successfully Registered',
            text: 'Congratulations!',
          }).then((result) => {
            if (result.isConfirmed) {
              history.push('/logIn');
            }
          });
        } else {
          Swal.fire({
            icon: 'error',
            title: 'Duplicate email found',
            text: 'Please provide a different email',
          });
        }
      })
      .catch((err) => {
        console.log('Error', err);
      });

    setFirstName('');
    setLastName('');
    setEmail('');
    setMobileNo('');
    setPassword1('');
    setPassword2('');
  }

  const retrieveUserDetails = (token) => {
    fetch('http://localhost:4000/users/details', {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);

        setUser({
          id: data._id,
          isAdmin: data.isAdmin,
        });
      });
  };

  useEffect(() => {
    if (
      firstName !== 'null' &&
      lastName !== 'null' &&
      email !== 'null' &&
      mobileNo !== '' &&
      password1 !== '' &&
      password2 !== '' &&
      password1 === password2
    ) {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [firstName, lastName, email, mobileNo, password1, password2]);

  return user.id != null ? (
    <Redirect to="/logIn" />
  ) : (
    <div className="registerUser">
      <img src={flower} alt="flower.png" />
      <div className="form">
        <h3>Create an Account</h3>

        <Form className="m-5 p-5" onSubmit={(e) => registerUser(e)}>
          <Row className="pb-3 mb-3">
            <Form.Group as={Col} controlId="userFirstName">
              <Form.Label>First Name</Form.Label>
              <Form.Control
                type="text"
                value={firstName}
                placeholder="Enter first name"
                onChange={(e) => setFirstName(e.target.value)}
                required
              />
            </Form.Group>

            <Form.Group as={Col} controlId="userLastName">
              <Form.Label>Last Name</Form.Label>
              <Form.Control
                type="text"
                value={lastName}
                placeholder="Enter last name"
                onChange={(e) => setLastName(e.target.value)}
                required
              />
            </Form.Group>
          </Row>

          <Form.Group className="pb-3 mb-3" controlId="userEmail">
            <Form.Label>Email Address</Form.Label>
            <Form.Control
              type="email"
              placeholder="Enter email address"
              value={email}
              onChange={(e) => setEmail(e.target.value)}
              required
            />
          </Form.Group>

          <Form.Group className="pb-3 mb-3" controlId="userMobileNo">
            <Form.Label>Mobile Number</Form.Label>
            <Form.Control
              type="number"
              placeholder="Enter mobile number"
              value={mobileNo}
              onChange={(e) => setMobileNo(e.target.value)}
              required
            />
          </Form.Group>

          <Row className="pb-3 mb-3">
            <Form.Group as={Col} controlId="password1">
              <Form.Label>Password</Form.Label>
              <Form.Control
                type="password"
                placeholder="Enter password"
                value={password1}
                onChange={(e) => setPassword1(e.target.value)}
                required
              />
            </Form.Group>

            <Form.Group className="p-0" as={Col} controlId="password2">
              <Form.Label>Confirm Password</Form.Label>
              <Form.Control
                type="password"
                placeholder="Confirm Password"
                value={password2}
                onChange={(e) => setPassword2(e.target.value)}
                required
              />
            </Form.Group>
          </Row>
          <div className="d-flex">
            {isActive ? (
              <Button className="m-auto p-3" variant="primary" type="submit">
                Register
              </Button>
            ) : (
              <Button
                className="m-auto p-3"
                variant="danger"
                type="submit"
                id="submitBtn"
                disabled
              >
                Register
              </Button>
            )}
          </div>
        </Form>
      </div>
    </div>
  );
};

export default Register;
