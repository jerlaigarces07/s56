import React from 'react';
import { Card, Button, Navbar, Container, Nav, Link } from 'react-bootstrap';
import random1 from '../images/random1.png';
import random2 from '../images/random2.jpg';
import random3 from '../images/random3.jpg';

const ProductList = () => {
  return (
    <>
      <div className="productList">
        <Navbar className="nav" bg="primary" variant="dark">
          <Container>
            <Navbar.Brand href="#home">Logo</Navbar.Brand>
            <Nav>
              <Nav.Link href="#features">Cart</Nav.Link>
              <Nav.Link href="#pricing">Account</Nav.Link>
            </Nav>
          </Container>
        </Navbar>
        <br />
        <div className="cards">
          <Card style={{ width: '18rem', margin: 'auto' }}>
            <Card.Img style={{ width: 'auto' }} variant="top" src={random3} />
            <Card.Body>
              <Card.Title>Product Name</Card.Title>
              <Card.Text>
                Lorem ipsum dolor sit amet consectetur adipisicing elit.
                Provident, at!
              </Card.Text>
              <Button variant="primary">Order Now!</Button>
            </Card.Body>
          </Card>

          <Card style={{ width: '18rem', margin: 'auto' }}>
            <Card.Img
              style={{ height: '17.6rem' }}
              variant="top"
              src={random1}
            />
            <Card.Body>
              <Card.Title>Product Name</Card.Title>
              <Card.Text>
                Lorem ipsum dolor sit amet consectetur adipisicing elit. Soluta,
                officiis!
              </Card.Text>
              <Button variant="primary">Order Now!</Button>
            </Card.Body>
          </Card>

          <Card style={{ width: '18rem', margin: 'auto' }}>
            <Card.Img
              style={{ height: '17.6rem' }}
              variant="top"
              src={random2}
            />
            <Card.Body>
              <Card.Title>Product Name</Card.Title>
              <Card.Text>
                Lorem ipsum dolor, sit amet consectetur adipisicing elit. Neque,
                nulla!
              </Card.Text>
              <Button variant="primary">Order Now!</Button>
            </Card.Body>
          </Card>
        </div>
      </div>
    </>
  );
};

export default ProductList;
