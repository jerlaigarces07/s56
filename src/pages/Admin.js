import React from 'react';
import { Card, Button, Navbar, Container, Nav, Link } from 'react-bootstrap';
import random2 from '../images/random2.jpg';

const Admin = () => {
  return (
    <>
      <div className="adminPage">
        <Navbar className="nav" bg="primary" variant="dark">
          <Container>
            <Navbar.Brand href="#home">Logo</Navbar.Brand>
            <Nav>
              <Nav.Link href="#pricing">Account</Nav.Link>
            </Nav>
          </Container>
        </Navbar>

        <div className="cardBody">
          <Card style={{ width: '18rem' }}>
            <Card.Img style={{ width: 'auto' }} variant="top" src={random2} />
            <Card.Body>
              <Button variant="primary">Manage Products</Button>
            </Card.Body>
          </Card>
        </div>
      </div>
    </>
  );
};

export default Admin;
