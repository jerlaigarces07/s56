import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import Register from './pages/Register';
import { Container } from 'react-bootstrap';
import LogIn from './pages/LogIn';
import ProductList from './pages/ProductList';
import Admin from './pages/Admin';
import { Switch } from 'react-router-dom';
import { BrowserRouter as Router } from 'react-router-dom';

function App() {
  return (
    <Router>
      <Container>
        <Switch>
          <Register exact path="/register" component={Register} />
          <LogIn exact path="/login" component={LogIn} />
          <ProductList />
          <Admin />
        </Switch>
      </Container>
    </Router>
  );
}

export default App;
